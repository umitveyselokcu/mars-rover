﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using static System.Console;

namespace mars_rover
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine(" Mars Rovers! ");
            WriteLine("--------------");
            
            WriteLine("Enter plateau grid size.");
            var gridSizeInput = ReadLine();
            Grid.Init(gridSizeInput);
            
            WriteLine();
            WriteLine("--------------");
            WriteLine("-CONTROLS-");
            WriteLine("'restart' to start again");
            WriteLine("'print' to print all rover positions on current grid.");
            WriteLine();
            

            while (string.IsNullOrEmpty(Grid.Error))
            {
                WriteLine();
                WriteLine("Enter initial position information to land a new rover.");
                var roverInitialPositionInput = ReadLine();
                if(ShouldRefresh(roverInitialPositionInput)) continue;
                
                var rover = new Rover(roverInitialPositionInput);
                if(!string.IsNullOrEmpty(rover.Error)) Grid.Restart();
                WriteLine();
                WriteLine("Enter rover command.");
                var roverCommand = ReadLine();
                if(ShouldRefresh(roverCommand)) continue;
                if (roverCommand != null) rover.Execute(roverCommand);
                WriteLine();
                WriteLine("Last position of current rover");
                WriteLine($"{rover.CoordinatX} {rover.CoordinatY} {rover.Direction}");
                WriteLine();
                Grid.Rovers.Add(rover);
                WriteLine($"You have {Grid.Rovers.Count} Rover on Mars! To print all rover positions just write 'print' and press enter.");
                WriteLine();
            }

        }

        private static bool ShouldRefresh(string input)
        {
            var shouldRefresh = false;
            if (input.Equals("restart", StringComparison.InvariantCultureIgnoreCase))
            {
                Grid.Restart();
                shouldRefresh = true;
            }

            if (string.IsNullOrEmpty(input))
            {
                shouldRefresh = true;
            }

            if (input.Equals("print",  StringComparison.InvariantCultureIgnoreCase))
            {
                Grid.PrintRoverLocations();
                shouldRefresh = true;
            }
            return shouldRefresh;
        }
    }


    public static class Grid
    {
        public const int MinX = 0;
        public const int MinY = 0;
        public static int MaxX { get; set; }
        public static int MaxY { get; set; }
        public static IList<Rover> Rovers { get; set; }

        public static string Error { get; set; }
        public static void Init(string gridSizeInput)
        {
            string[] gridSize = {};
            if (!string.IsNullOrEmpty(gridSizeInput)) gridSize =  gridSizeInput.Split(" ");
            if ( gridSize.Length < 2)
            {
                WriteLine("Invalid input.");
                Restart();
            }
            MaxX = Convert.ToInt32(gridSize[0]);
            MaxY = Convert.ToInt32(gridSize[1]);
            WriteLine();
            WriteLine($"{MaxX}x{MaxY} grid initiated.");
            Rovers = new List<Rover>();
        }

        public static void Restart()
        {
            Rovers = new List<Rover>();
            MaxX = 0;
            MaxY = 0;
            WriteLine("Grid is restarted.");
            WriteLine();
            WriteLine("Enter new grid size. like: '5 6'");
            Init(ReadLine());
            WriteLine();
        }

        public static void PrintRoverLocations()
        {
            WriteLine();
            WriteLine("All rover positions:");
            WriteLine();
            foreach (var rover in Rovers)
            {
                WriteLine($"{rover.CoordinatX} {rover.CoordinatY} {rover.Direction}");
                
            }

        }
        
        
    }
    public class Rover
    {
        public int CoordinatX { get; set; }
        public int CoordinatY { get; set; }
        public Compass Direction { get; set; }
        
        public string Error { get; set; }
        public Rover(string roverInitialPositionInput)
        {
            var inputs = roverInitialPositionInput.Split(" ", StringSplitOptions.RemoveEmptyEntries);
            if (inputs.Length == 3)
            {
                CoordinatX = Convert.ToInt32(inputs[0]);
                CoordinatY = Convert.ToInt32(inputs[1]);
                Direction = ParseToEnum.GetEnum<Compass>( inputs[2].ToString());
            }
            else
            {
                Error = "Invalid Input - App is restarting";
            }
        }

        private void Move()
        {
            int newCoordinate;
            switch (Direction)
            {
                case Compass.N:
                    newCoordinate = CoordinatY + 1;
                    if (newCoordinate <= Grid.MaxY && newCoordinate >= Grid.MinY)
                        CoordinatY = newCoordinate;
                    break;
                case Compass.W:
                    newCoordinate = CoordinatX - 1;
                    if (newCoordinate <= Grid.MaxX && newCoordinate >= Grid.MinX)
                        CoordinatX = newCoordinate;
                    break;
                case Compass.S:
                    newCoordinate = CoordinatY - 1;
                    if (newCoordinate <= Grid.MaxY && newCoordinate >= Grid.MinY)
                        CoordinatY = newCoordinate;
                    break;
                case Compass.E:
                    newCoordinate = CoordinatX + 1;
                    if (newCoordinate <= Grid.MaxX && newCoordinate >= Grid.MinX)
                        CoordinatX = newCoordinate;
                    break;
            }
        }

        private void Rotate(Rotate rotation)
        {
            switch (rotation)
            {
                case mars_rover.Rotate.L:
                    if (Direction == Compass.E)
                    {
                        Direction = (Compass) 0;
                    }
                    else
                    {
                        Direction = (Compass)((int) Direction + 1);
                    }
                    break;
                case mars_rover.Rotate.R:
                    if (Direction == Compass.N)
                    {
                        Direction = (Compass) 3;
                    }
                    else
                    {
                        Direction = (Compass)((int) Direction - 1);
                    }
                    break;
            }
        }

        public void Execute(string command)
        {
            foreach (var letter in command)
            {
                if (letter.ToString().Equals("M", StringComparison.InvariantCultureIgnoreCase))
                {
                    Move();
                    continue;
                }
                Rotate(ParseToEnum.GetEnum<Rotate>( letter.ToString()));
            }
        }
    }

    public enum Compass
    {
        N = 0,
        W = 1,
        S = 2,
        E = 3
    }
    public enum Rotate
    {
        L,
        R
    }
    
    public static class ParseToEnum
    {
        public static  T GetEnum<T>(string str) where T : struct, IConvertible
        {
            var enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new Exception("T must be an Enumeration type.");
            }
            return Enum.TryParse<T>(str, true, out var val) ? val : default(T);
        }
    }
 
}